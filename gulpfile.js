const gulp = require('gulp');
const run = require('run-sequence');
const rename = require('gulp-rename');
const del = require('del');

const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cleanCSS = require('gulp-clean-css');

const posthtml = require('gulp-posthtml');
const include = require('posthtml-include');

const babel = require('gulp-babel');
const concat = require('gulp-concat');
const jsmin = require('gulp-uglify');

const imagemin = require('gulp-imagemin');

const plumber = require('gulp-plumber');
const server = require('browser-sync').create();

let jsList = [
  'source/js/**/*.js'
];

let postCssPlugins = [
  autoprefixer(),
];

gulp.task('html', function () {
  return gulp.src([
    'source/**/*.html',
    '!source/html/components/*.html'])
    .pipe(plumber())
    .pipe(posthtml([
      include()
    ]))
    .pipe(gulp.dest('public/'))
    .pipe(server.stream());
});

gulp.task('style', function () {
  gulp.src('source/sass/style.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(postcss(postCssPlugins))
    .pipe(gulp.dest('public/css'))
    .pipe(cleanCSS())
    .pipe(rename('style.min.css'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'))
    .pipe(server.stream());
});

gulp.task('script', function () {
  return gulp.src(jsList)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(concat('script.js'))
    .pipe(gulp.dest('public/js'))
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js'))
    .pipe(server.stream());
});

gulp.task('images', function () {
  return gulp.src('source/img/**/*.{png,jpg,svg}')
    .pipe(imagemin([
      imagemin.optipng({optimizationLevel: 3}),
      imagemin.jpegtran({progressive: true}),
    ]))
    .pipe(rename({dirname: ''}))
    .pipe(gulp.dest('public/img'))
    .pipe(server.stream());
});


gulp.task('serve', function () {
  server.init({
    server: 'public/',
    notify: false,
    open: true,
    cors: true,
    ui: false
  });

  gulp.watch('source/*.html', ['html']);
  gulp.watch('source/sass/**/*.{scss,sass}', ['style']);
  gulp.watch('source/js/**/*.js', ['script']);
  gulp.watch('source/img/**/*.{png,jpg,svg}', ['images']);
});

gulp.task('copy', function () {
  return gulp.src('source/*.html', {
    base: 'source'
  })
  .pipe(gulp.dest('public'));
});

gulp.task('clean', function () {
  return del('public');
});

gulp.task('build', function (done) {
  run(
      'clean',
      'copy',
      'style',
      'script',
      'images',
      done
  );
});
