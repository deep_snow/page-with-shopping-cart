(function () {
  if (document.querySelector('.basket') && document.querySelector('.product')) {
    const DEBOUNCE_INTERVAL = 1000;
    let lastTimeout;
    const orderItemTemplate = document.querySelector('template').content.querySelector('.basket__item');
    const basketNode = document.querySelector('.basket');
    const basketOrderTable = basketNode.querySelector('.basket__order');
    let order = [];

    function debounce(fun) {
      if (lastTimeout) {
        window.clearTimeout(lastTimeout);
      }
      lastTimeout = setTimeout(fun, DEBOUNCE_INTERVAL);
    }

    /**
     * Сохраняет в localStorage список товаров из корзины
     * @param {Array} itemsList - список товаров
     */
    function saveOrder(itemsList) {
      debounce(() => {
        localStorage.setItem('order', JSON.stringify(itemsList));
      });
    }

    /**
     * Добавляет (отрисовывает) товар в корзину
     * @param {Object} item - товар
     */
    function addToOrder(item) {
      const orderItem = orderItemTemplate.cloneNode(true);
      orderItem.querySelector('.basket__product-name').textContent = item.name;
      orderItem.querySelector('.basket__price').textContent = item.price;
      orderItem.querySelector('.basket__delete button').addEventListener('click', () => {
        order.splice(order.indexOf(item), 1);
        orderItem.remove();
        saveOrder(order);
        basketNode.querySelector('.basket__num').innerText = calcTotalSum(order) + ' руб.';
      });
      order.push(item);
      basketOrderTable.appendChild(orderItem);
      saveOrder(order);
      basketNode.querySelector('.basket__num').innerText = calcTotalSum(order) + ' руб.';
    }

    /**
     *  Расчитывает общую сумму товаров из корзины и выводит в
     * @param {Array} itemsList список товаров добавленных в корзину
     */
    function calcTotalSum(itemsList) {
      let result = itemsList.reduce(function (sum, current) {
        let currentPrice = parseInt(current.price, 10);
        return sum + currentPrice;
      }, 0);

      if (result === 0) return result.toFixed(2);
      return result;
    }

    /**
     * Восстанавливает состояние корзины из localStorage
     */
    function updateBasket() {
      if (localStorage.getItem('order')) {
        let savedOrder = JSON.parse(localStorage.getItem('order'));
        savedOrder.forEach((item) => {
          addToOrder(item);
        });
      }
    }

    /**
     * Добавляет обработчик события для кнопки "В корзину!"
     * для всех продуктов на странице
     */
    function addHandlerForProducts() {
      document.querySelectorAll('.product').forEach((element) => {
        let product = {};
        product.name = element.querySelector('.product__name').innerHTML;
        product.price = element.querySelector('.product__price').innerText;
        element.querySelector('.product__btn').addEventListener('click', () => {
          addToOrder(product);
        });
      });
    }


    addHandlerForProducts();
    updateBasket();
    basketNode.querySelector('.basket__btn').addEventListener('click', () => {
      if (order.length === 0) {
        alert('В корзине ничего нет.');
      } else {
        let itemNameList = order.map((item) => {
          return '"' + item.name + '"';
        });
        alert('Вы добавили в корзину ' + itemNameList.join(', ') + ' на сумму ' + calcTotalSum(order) + ' руб.');
      }
    });
  }
})();
