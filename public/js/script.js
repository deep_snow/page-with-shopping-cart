'use strict';

(function () {
  if (document.querySelector('.basket') && document.querySelector('.product')) {
    var debounce = function debounce(fun) {
      if (lastTimeout) {
        window.clearTimeout(lastTimeout);
      }
      lastTimeout = setTimeout(fun, DEBOUNCE_INTERVAL);
    };

    /**
     * Сохраняет в localStorage список товаров из корзины
     * @param {Array} itemsList - список товаров
     */


    var saveOrder = function saveOrder(itemsList) {
      debounce(function () {
        localStorage.setItem('order', JSON.stringify(itemsList));
      });
    };

    /**
     * Добавляет (отрисовывает) товар в корзину
     * @param {Object} item - товар
     */


    var addToOrder = function addToOrder(item) {
      var orderItem = orderItemTemplate.cloneNode(true);
      orderItem.querySelector('.basket__product-name').textContent = item.name;
      orderItem.querySelector('.basket__price').textContent = item.price;
      orderItem.querySelector('.basket__delete button').addEventListener('click', function () {
        order.splice(order.indexOf(item), 1);
        orderItem.remove();
        saveOrder(order);
        basketNode.querySelector('.basket__num').innerText = calcTotalSum(order) + ' руб.';
      });
      order.push(item);
      basketOrderTable.appendChild(orderItem);
      saveOrder(order);
      basketNode.querySelector('.basket__num').innerText = calcTotalSum(order) + ' руб.';
    };

    /**
     *  Расчитывает общую сумму товаров из корзины и выводит в
     * @param {Array} itemsList список товаров добавленных в корзину
     */


    var calcTotalSum = function calcTotalSum(itemsList) {
      var result = itemsList.reduce(function (sum, current) {
        var currentPrice = parseInt(current.price, 10);
        return sum + currentPrice;
      }, 0);

      if (result === 0) return result.toFixed(2);
      return result;
    };

    /**
     * Восстанавливает состояние корзины из localStorage
     */


    var updateBasket = function updateBasket() {
      if (localStorage.getItem('order')) {
        var savedOrder = JSON.parse(localStorage.getItem('order'));
        savedOrder.forEach(function (item) {
          addToOrder(item);
        });
      }
    };

    /**
     * Добавляет обработчик события для кнопки "В корзину!"
     * для всех продуктов на странице
     */


    var addHandlerForProducts = function addHandlerForProducts() {
      document.querySelectorAll('.product').forEach(function (element) {
        var product = {};
        product.name = element.querySelector('.product__name').innerHTML;
        product.price = element.querySelector('.product__price').innerText;
        element.querySelector('.product__btn').addEventListener('click', function () {
          addToOrder(product);
        });
      });
    };

    var DEBOUNCE_INTERVAL = 1000;
    var lastTimeout = void 0;
    var orderItemTemplate = document.querySelector('template').content.querySelector('.basket__item');
    var basketNode = document.querySelector('.basket');
    var basketOrderTable = basketNode.querySelector('.basket__order');
    var order = [];

    addHandlerForProducts();
    updateBasket();
    basketNode.querySelector('.basket__btn').addEventListener('click', function () {
      if (order.length === 0) {
        alert('В корзине ничего нет.');
      } else {
        var itemNameList = order.map(function (item) {
          return '"' + item.name + '"';
        });
        alert('Вы добавили в корзину ' + itemNameList.join(', ') + ' на сумму ' + calcTotalSum(order) + ' руб.');
      }
    });
  }
})();